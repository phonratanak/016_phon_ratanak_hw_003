package HourlyEmployer;
import StaffMember.*;
public class HourlyEmployer extends StaffMember{
    private int hourWorked;
    private double rate;
    public HourlyEmployer(){}
    public HourlyEmployer(int id,String name,String address,int hourWorked,double rate)
    {
        super(id,name,address);
        this.hourWorked=hourWorked;
        this.rate=rate;
    }
    public double getRate() {
        return rate;
    }
    public int getHourWorked() {
        return hourWorked;
    }
    public void setHourWorked(int hourWorked) {
        this.hourWorked = hourWorked;
    }
    public void setRate(double rate) {
        this.rate = rate;
    }
    public String getTitle()
    {
        return "HourlyEmployer";
    }
    @Override
    public String toString() {
        return super.toString() +
                "HourWorked: " + hourWorked +
                " Rate: " + rate+" Payment: "+(this.pay()==-1?null:this.pay())+" ("+this.getTitle()+")";
    }
    @Override
    public double pay() {
        if(hourWorked <= 0 || rate <=0)
        {
            return -1;
        }
        else
        {
            return hourWorked*rate;
        }

    }
}
