import  java.lang.*;
import java.util.ArrayList;
import java.util.*;

import Volunteer.*;
import SalariedEmployer.*;
import StaffMember.*;
import HourlyEmployer.*;
import java.util.Scanner;
import java.lang.String;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;
import org.nocrala.tools.texttablefmt.CellStyle.HorizontalAlign;
public class Main {
    public static ArrayList storeValue;
    public static StaffMember objStaff[];
    public static Scanner sc=new Scanner(System.in);
    public Scanner sca=new Scanner(System.in);
    public void StartObject()
    {
        CellStyle numberStyle = new CellStyle(HorizontalAlign.left);
        Table tr = new Table(1, BorderStyle.UNICODE_HEAVY_BOX_WIDE, ShownBorders.ALL);
        tr.setColumnWidth(0,40,200);

        storeValue.sort(new NameSorter());
        for(int i=0; i<storeValue.size();i++)
        {
            tr.addCell(storeValue.get(i).toString(),numberStyle);
        }
        System.out.println(tr.render());
    }
    public void addVolunteer()
    {
        int id;
        String name,address;
        Scanner scan1=new Scanner(System.in);
        try {
            System.out.print("-> Enter Staff ID: ");
            id=scan1.nextInt();
            System.out.print("-> Enter Name: ");
            name=sca.nextLine();
            System.out.print("-> Enter Address: ");
            address=sca.nextLine();
            String n=name.substring(0,1);
            String str=n.toUpperCase();
            String strExcludeFirstLetter = name.substring(1, name.length());
            storeValue.add(new Volunteer(id,(str+strExcludeFirstLetter),address));
        }catch (Exception e)
        {
            System.out.println();
            System.out.println("Input Not valid");
        }
    }
    public void SalaryEdp()
    {
        int id;
        String name,address;
        double salary,bonus;
        Scanner scan=new Scanner(System.in);
        try {
            System.out.print("-> Enter Staff ID: ");
            id=scan.nextInt();
            System.out.print("-> Enter Name: ");
            name=sca.nextLine();
            System.out.print("-> Enter Address: ");
            address=sca.nextLine();
            System.out.print("-> Enter Salary: ");
            salary=scan.nextDouble();
            System.out.print("-> Enter Bonus: ");
            bonus=scan.nextDouble();
            String n=name.substring(0,1);
            String str=n.toUpperCase();
            String strExcludeFirstLetter = name.substring(1, name.length());
            storeValue.add(new SalariedEmployer(id,(str+strExcludeFirstLetter),address,salary,bonus));
        }catch (Exception e)
        {
            System.out.println();
            System.out.println("Input Not valid");
        }
    }
    public void HourlyEmp()
    {
        int id;
        String name,address;
        double hour,rate;
        Scanner scan=new Scanner(System.in);
        try{
            System.out.print("-> Enter Staff ID: ");
            id=scan.nextInt();
            System.out.print("-> Enter Name: ");
            name=sca.nextLine();
            System.out.print("-> Enter Address: ");
            address=sca.nextLine();
            System.out.print("-> Enter Hourly: ");
            hour=scan.nextDouble();
            System.out.print("-> Enter Rate: ");
            rate=scan.nextDouble();
            String n=name.substring(0,1);
            String str=n.toUpperCase();
            String strExcludeFirstLetter = name.substring(1, name.length());
            storeValue.add(new SalariedEmployer(id,(str+strExcludeFirstLetter),address,hour,rate));
        }catch (Exception e)
        {
            System.out.println();
            System.out.println("Input Not valid");
        }
    }
    public void updateAllMember()
    {
        int id,hour;
        String name,address;
        boolean check=true;
        double rate;
        Scanner scan=new Scanner(System.in);
        Scanner up=new Scanner(System.in);
        CellStyle numberStyle = new CellStyle(HorizontalAlign.left);
        Table trs1 = new Table(1, BorderStyle.UNICODE_HEAVY_BOX_WIDE, ShownBorders.ALL);
        trs1.setColumnWidth(0,40,200);
        try{
            System.out.print("=> Enter ID you want to Update:");
            id=scan.nextInt();
            Iterator<StaffMember> catIterator = storeValue.iterator();
            while(catIterator.hasNext()) {
                StaffMember nextCat=catIterator.next();
                if (nextCat.getId()==id) {
                    String className=nextCat.getClass().getSimpleName();
                    if(nextCat instanceof Volunteer)
                    {
                        Scanner vol=new Scanner(System.in);
                        Scanner put=new Scanner(System.in);
                        Table trs3 = new Table(1, BorderStyle.UNICODE_HEAVY_BOX_WIDE, ShownBorders.ALL);
                        trs3.setColumnWidth(0,40,200);
                        trs3.addCell(nextCat.toString(),numberStyle);
                        System.out.println(trs3.render());
                        try {
                            System.out.print("-> Enter Name: ");
                            name=vol.nextLine();
                            System.out.print("-> Enter Address: ");
                            address=vol.nextLine();
                            String n=name.substring(0,1);
                            String str=n.toUpperCase();
                            String strExcludeFirstLetter = name.substring(1, name.length());
                            nextCat.setName(str+strExcludeFirstLetter);
                            nextCat.setAddress(address);
                            check=false;
                        }catch (Exception e)
                        {
                            System.out.println();
                            System.out.println("Input Not valid");
                        }
                    }else if(className.equals("SalariedEmployer"))
                    {
                        double salary,bonus;
                        Table trs2 = new Table(1, BorderStyle.UNICODE_HEAVY_BOX_WIDE, ShownBorders.ALL);
                        trs2.setColumnWidth(0,40,200);
                        trs2.addCell(nextCat.toString(),numberStyle);
                        System.out.println(trs2.render());
                        try {
                            System.out.print("-> Enter Name: ");
                            name=up.nextLine();
                            System.out.print("-> Enter Address: ");
                            address=up.nextLine();
                            System.out.print("-> Enter Salary: ");
                            salary=up.nextDouble();
                            System.out.print("-> Enter Bonus: ");
                            bonus=up.nextDouble();
                            String n=name.substring(0,1);
                            String str=n.toUpperCase();
                            String strExcludeFirstLetter = name.substring(1, name.length());
                            nextCat.setName(str+strExcludeFirstLetter);
                            nextCat.setAddress(address);
                            ((SalariedEmployer) nextCat).setSalary(salary);
                            ((SalariedEmployer) nextCat).setBonus(bonus);
                            check=false;
                        }catch (Exception e)
                        {
                            System.out.println();
                            System.out.println("Input Not valid");
                        }
                    }
                    else{
                        trs1.addCell(nextCat.toString(),numberStyle);
                        System.out.println(trs1.render());
                        try {
                            System.out.print("-> Enter Name: ");
                            name=up.nextLine();
                            System.out.print("-> Enter Address: ");
                            address=up.nextLine();
                            System.out.print("-> Enter Hourly: ");
                            hour=up.nextInt();
                            System.out.print("-> Enter Rate: ");
                            rate=up.nextDouble();
                            String n=name.substring(0,1);
                            String str=n.toUpperCase();
                            String strExcludeFirstLetter = name.substring(1, name.length());
                            nextCat.setName(str+strExcludeFirstLetter);
                            nextCat.setAddress(address);
                            ((HourlyEmployer) nextCat).setHourWorked(hour);
                            ((HourlyEmployer) nextCat).setRate(rate);
                            check=false;
                        }catch (Exception e)
                        {
                            System.out.println();
                            System.out.println("Input Not valid");
                        }
                    }
                }
            }
        }catch (Exception e)
        {
            System.out.println("Input Invalid");
        }
        if(check)
        {
            System.out.println();
            System.out.println("ID is invalid! please try again");
        }
    }
    public void removeEmp(){
        int id;
        boolean check=true;
        Scanner pr=new Scanner(System.in);
        CellStyle numberStyle = new CellStyle(HorizontalAlign.left);
        Table trs = new Table(1, BorderStyle.UNICODE_HEAVY_BOX_WIDE, ShownBorders.ALL);
        trs.setColumnWidth(0,40,200);
        try
        {
            System.out.print("Enter Staff ID to Remove:");
            id=pr.nextInt();
            Iterator<StaffMember> catIterator = storeValue.iterator();// Create an iterator
            while(catIterator.hasNext()) {// As long as there are elements in the list
                StaffMember nextCat = catIterator.next();// Get the next element
                if (nextCat.getId()==id) {
                    System.out.println(nextCat);
                    trs.addCell(nextCat.toString(),numberStyle);
                    catIterator.remove();
                    System.out.println(trs.render());
                    System.out.println("Remove Successfully");
                    check=false;
                }
            }
            if(check)
            {
                System.out.println();
                System.out.println("ID is invalid! please try again");
            }
        }catch (Exception e)
        {
            System.out.println("Input is Not Valid");
        }
    }
    public static void main(String[] args)throws CloneNotSupportedException  {
        String choose;
        String tbl="===================================================================================";
        Main obj=new Main();
        objStaff=new StaffMember[3];
        storeValue=new ArrayList<>();
        Scanner input=new Scanner(System.in);
        objStaff[0] =new Volunteer(1,"Phon Ratanak","phnom penh");
        objStaff[1]=new SalariedEmployer(2,"Ahan Monoreth","bangtay meanchey",1200.0,3);
        objStaff[2]=new HourlyEmployer(3,"Bim sok","babtom bong",4, 3);
        for(int i=0;i<3;i++)
        {
            storeValue.add(objStaff[i]);
        }
        do{
            obj.StartObject();
            System.out.println("1.Add Employee");
            System.out.println("2.Update Employee");
            System.out.println("3.Remove Employee");
            System.out.println("4.Exit");
            System.out.print("=> Choose Option [1-4]:");
            choose=sc.nextLine();
            switch (choose)
            {
                case "1":
                    String add;
                    Scanner input1=new Scanner(System.in);
                    System.out.println(tbl);
                    System.out.println("1.Add Volunteer | 2.Add SalariedEmployee | 3.Add HourlyEmployee | 4.Break ");
                    System.out.println(tbl);
                    System.out.print("=> Choose Option :");
                    add=input1.nextLine();
                    switch (add)
                    {
                        case "1":
                            obj.addVolunteer();
                            break;
                        case "2":
                            obj.SalaryEdp();
                            break;
                        case "3":
                            obj.HourlyEmp();
                            break;
                        case "4":
                            break;
                        default:
                            System.out.println("Input Value is Wrong! please try again");
                            break;
                    }
                    break;
                case "2":
                    String adds;
                    System.out.println("============================");
                    System.out.println("  Update All Staff Member ");
                    System.out.println("============================");
                    obj.updateAllMember();
                    break;
                case "3":
                    System.out.println("============================");
                    System.out.println("  Remove All Staff Member ");
                    System.out.println("============================");
                    obj.removeEmp();
                    break;
                case "4":
                    System.out.println("Application Exit!");
                    System.exit(0);
                default:
                    System.out.println("Input Value is Wrong! please try again");
            }
        }while (choose !="4");
    }
}
