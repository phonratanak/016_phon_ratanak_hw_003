import StaffMember.StaffMember;

import java.util.Comparator;

public class NameSorter implements Comparator<StaffMember> {
    @Override
    public int compare(StaffMember o1, StaffMember o2) {
        return o1.getName().compareToIgnoreCase(o2.getName());
    }
}
