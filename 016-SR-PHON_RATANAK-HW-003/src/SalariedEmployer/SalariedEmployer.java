package SalariedEmployer;
import StaffMember.*;
public class SalariedEmployer extends StaffMember {
    private  double salary;
    private  double bonus;
    public SalariedEmployer(){}
    public SalariedEmployer(int id,String name,String address,double salary,double bonus)
    {
        super(id,name,address);
        this.salary=salary;
        this.bonus=bonus;
    }
    public double getBonus() {
        return bonus;
    }
    public double getSalary() {
        return salary;
    }
    public void setBonus(double bonus) {
        this.bonus = bonus;
    }
    public void setSalary(double salary) {
        this.salary = salary;
    }
    public String getTitle()
    {
        return "Salary Employer";
    }
    @Override
    public String toString() {
        return super.toString() +
                "Salary: " + salary +
                " Bonus: " + bonus +" Payment: "+(this.pay()==-1?null:this.pay())+" ("+this.getTitle()+")"
                ;
    }
    @Override
    public double pay() {
        if(salary <= 0 || bonus <=0)
        {
            return -1;
        }
        else
        {
            return salary*bonus;
        }

    }
}
