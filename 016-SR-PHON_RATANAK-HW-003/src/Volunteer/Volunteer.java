package Volunteer;
import StaffMember.*;
public class Volunteer extends StaffMember{
    public Volunteer(){}
    public Volunteer(int id,String name,String address)
    {
        super(id,name,address);
    }

    @Override
    public String toString() {
        return super.toString()+ ", Thank!"+" ("+this.getTitle()+")";
    }

    @Override
    public double pay() {
        return 0;
    }
    public String getTitle()
    {
        return "Volunteer";
    }
}
